//
//  QWWalletViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 06/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "QWWalletViewController.h"
#import "QWActivityHelper.h"
#import "QWSdk.h"
#import "NSString+FormValidation.h"
#import "QWConsumerConstants.h"
#import "selectpaymenttypeViewController.h"

@interface QWWalletViewController ()

@end

@implementation QWWalletViewController

- (void) viewWillAppear:(BOOL)animated{
    [self getWalletDetails];
    
    if (!IsEmpty([self.data objectForKey:@"paymentamount"])) {
        self.minimumBalanceLbl.text = [NSString stringWithFormat:@"Minimum Balance - ₹%@", [self.data objectForKey:@"paymentamount"]];
        
        self.Btn1.enabled = NO;
        self.Btn2.enabled = NO;
        self.Btn3.enabled = NO;
        self.Btn4.enabled = NO;
        self.amountField.text = [self.data objectForKey:@"paymentamount"];
    } else{
        self.minimumBalanceLbl.hidden = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"ADD MONEY";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(popBack)];
    
    if (!IsEmpty([self.data objectForKey:@"type"]) && [[self.data objectForKey:@"type"] isEqualToString:@"recharge"]) {
        // Do something else
    } else{
        [self setDisconnectBtn];
    }
    
    self.amountField.delegate = self;
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.amountField.inputAccessoryView = keyboardDoneButtonView;
}

-(void) popBack {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void) getWalletDetails{
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)(id) = ^void(id  data){
        [QWActivityHelper removeActivityIndicator:self.view];
        if (!IsEmpty([data objectForKey:@"balance"])) {
            self.walletBalanceLbl.text = [NSString stringWithFormat:@"₹ %@", [data objectForKey:@"balance"]];
        }
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw checkbalance :data :success :failure];
}

- (void) setDisconnectBtn {
    UIImage *image = [[UIImage imageNamed:@"disconnect.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *disconnectBtn = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(disconnect:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:disconnectBtn, nil];
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (NSString *)validateForm {
    NSString *errorMessage;
    
    if (!IsEmpty([self.data objectForKey:@"paymentamount"])) {
        if(![self.amountField.text isValidAmount] && [self.amountField.text floatValue] < [[self.data objectForKey:@"paymentamount"] floatValue])
        {
            errorMessage = @"Please enter a valid Amount";
        }
    }else{
        if(![self.amountField.text isValidAmount])
        {
            errorMessage = @"Please enter a valid Amount";
        }
    }
    
    return errorMessage;
}

- (void) disconnect :(id)sender{
    NSLog(@"DATA: ", self.data);
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)() = ^void(id  response){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
        [global removeObjectForKey:@"isConnected"];
        
        NSDictionary *resp = @{
            @"code": @"209",
            @"status": @"success"
            };
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc] initWithDictionary:resp];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:QW_WALLET_DISCONNECT_NOTIFICATION object:self userInfo:responseData];
    };
    
    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw disconnect :data :success :failure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)recharge:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSString *errorMessage = [self validateForm];
    
    if(errorMessage){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message: errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else{
        
        NSDictionary * dict = [[NSDictionary alloc] init];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
        if (!IsEmpty([self.data objectForKey:@"mobile"])) {
            [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
        }
        if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
            [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
        }
        if (!IsEmpty([self.data objectForKey:@"signature"])) {
            [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
        }
        
        void(^success)(id) = ^void(id  response){
            [QWActivityHelper removeActivityIndicator:self.view];
            
            selectpaymenttypeViewController *selectPayment  = [[selectpaymenttypeViewController alloc] init];
            selectPayment.data = self.data;
            selectPayment.modes = [response objectForKey:@"modes"];
            selectPayment.amount = self.amountField.text;
            [self.navigationController pushViewController:selectPayment animated:YES];
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        };
        
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw walletRechargeModes :data :success :failure];
    }
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (IBAction)handleBtnTap:(id)sender {
    
    [[self.view viewWithTag:111] removeFromSuperview];
    
    UIButton *btn = sender;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, btn.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:0.00 green:0.20 blue:0.40 alpha:1.0];
    lineView.tag = 111;
    [btn addSubview:lineView];
    
    if ([sender tag] == 1) {
        self.amountField.text = @"100";
    } else if ([sender tag] == 2) {
        self.amountField.text = @"200";
    } else if ([sender tag] == 3){
        self.amountField.text = @"500";
    } else if ([sender tag] == 4){
        self.amountField.text = @"1000";
    }
}
@end
