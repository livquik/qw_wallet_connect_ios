//
//  selectpaymenttypeViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by pritam soni on 14/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "selectpaymenttypeViewController.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"
#import "QWConsumerConstants.h"
#import "QWWalletCardsViewController.h"
#import "netbankingViewController.h"

@interface selectpaymenttypeViewController ()

@end

@implementation selectpaymenttypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"MODE OF RECHARGE";
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(cardViewSelected:)];
    [self.cardView addGestureRecognizer:singleFingerTap];
    
    UITapGestureRecognizer *tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(NetbankingViewSelected:)];
    [self.netbankingView addGestureRecognizer:tap];
    
    if (![self.modes containsObject:@"cards"]) {
        self.cardView.hidden = YES;
    }
    
    if (![self.modes containsObject:@"netbanking"]) {
        self.netbankingView.hidden = YES;
    }
    
    if (!IsEmpty([self.data objectForKey:@"type"]) && [[self.data objectForKey:@"type"] isEqualToString:@"recharge"]) {
        // Do something else
    } else{
        [self setDisconnectBtn];
    }
}

- (void)cardViewSelected:(UITapGestureRecognizer *)recognizer
{
    QWWalletCardsViewController *cards = [[QWWalletCardsViewController alloc] init];
    cards.data = self.data;
    cards.amount = self.amount;
    [self.navigationController pushViewController:cards animated:YES];
    
}

- (void)NetbankingViewSelected:(UITapGestureRecognizer *)recognizer
{
    netbankingViewController *netbanking = [[netbankingViewController alloc] init];
    netbanking.data = self.data;
    netbanking.amount = self.amount;
    [self.navigationController pushViewController:netbanking animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setDisconnectBtn {
    UIImage *image = [[UIImage imageNamed:@"disconnect.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *disconnectBtn = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(disconnect:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:disconnectBtn, nil];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (void) disconnect :(id)sender{
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
       [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)(id ) = ^void(id  response){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
        [global removeObjectForKey:@"isConnected"];
        
        NSDictionary *resp = @{
                               @"code": @"209",
                               @"status": @"success"
                               };
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc] initWithDictionary:resp];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:QW_WALLET_DISCONNECT_NOTIFICATION object:self userInfo:responseData];
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw disconnect :data :success :failure];
}


@end
