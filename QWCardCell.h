//
//  QWCardCell.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 17/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWCardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ccno;
@property (weak, nonatomic) IBOutlet UIImageView *cardType;
@property (weak, nonatomic) IBOutlet UILabel *bankName;
@property (weak, nonatomic) IBOutlet UIView *logoContainerView;

@end
