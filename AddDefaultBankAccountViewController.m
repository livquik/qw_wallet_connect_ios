//
//  AddDefaultBankAccountViewController.m
//  QuikWallet
//
//  Created by Deep on 06/03/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import "AddDefaultBankAccountViewController.h"
#import "NSString+FormValidation.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"
#import "KYCSuccessViewController.h"

@interface AddDefaultBankAccountViewController ()

@end

@implementation AddDefaultBankAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    self.nameField.delegate = self;
    self.accNoField.delegate = self;
    self.confirmAccNoField.delegate = self;
    self.ifscField.delegate = self;
    
    self.nameField.inputAccessoryView = keyboardDoneButtonView;
    self.accNoField.inputAccessoryView = keyboardDoneButtonView;
    self.confirmAccNoField.inputAccessoryView = keyboardDoneButtonView;
    self.ifscField.inputAccessoryView = keyboardDoneButtonView;
    
    self.navigationItem.title = @"Add Bank A/C";
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 116 || textField.tag == 117){
        if (textField.text.length >= 20 && range.length == 0)
        {
            return NO;
        }
        else
        {
            YES;
        }
    }else if (textField.tag == 118){
        if (textField.text.length >= 11 && range.length == 0)
        {
            return NO;
        }
        else
        {
            YES;
        }
    }
    return YES;
}



- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (NSString *)validateBankTransferForm {
    NSString *errorMessage;
    
    if(![self.nameField.text isValidName])
    {
        errorMessage = @"Please enter a valid name";
    }
    else if(![self.accNoField.text isValidAccount])
    {
        errorMessage = @"Please enter a valid account number";
    }
    else if(![self.accNoField.text isEqualToString:self.confirmAccNoField.text])
    {
        errorMessage = @"Account Numbers do not match";
    }
    else if(![self.ifscField.text isValidIFSC])
    {
        errorMessage = @"Please enter a valid IFSC code";
    }
    
    return errorMessage;
}


- (IBAction)addDefaultAccount:(id)sender {
    NSString *errorMessage = [self validateBankTransferForm];
    
    if (!IsEmpty(errorMessage)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message: errorMessage
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:NO completion:nil];
        
    } else{
        [QWActivityHelper displayActivityIndicator:self.view];
        
        void(^success)(id) = ^void(id  data){
            [QWActivityHelper removeActivityIndicator:self.view];
            KYCSuccessViewController *kycSuccess = [[KYCSuccessViewController alloc] init];
            [self.navigationController pushViewController:kycSuccess animated:YES];
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        };
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.data];
        [data setObject:self.accNoField.text forKey:@"accnumber"];
        [data setObject:self.ifscField.text forKey:@"ifsc"];
        [data setObject:self.nameField.text forKey:@"nickname"];
        [data setObject:@"Y" forKey:@"updatekyc"];
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw addDefaultBeneficiary:data :success :failure];
    }
}

@end
