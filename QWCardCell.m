//
//  QWCardCell.m
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 17/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import "QWCardCell.h"

@implementation QWCardCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
