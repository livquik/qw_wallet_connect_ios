//
//  QWMobileViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 31/08/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "QWMobileViewController.h"
#import "QWActivityHelper.h"
#import "QWSdk.h"
#import "QWConnectViewController.h"

@interface QWMobileViewController ()

@end

@implementation QWMobileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        self.mobileNumberLbl.text = [self.data objectForKey:@"mobile"];
    }
    
    self.navigationItem.title = @"WALLET CONNECT";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)generateOTP:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }

    void(^success)(id) = ^void(id  data){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        QWConnectViewController *connect = [[QWConnectViewController alloc] init];
        connect.data = self.data;
        NSDictionary *userData = [data objectForKey:@"user"];
        if (!IsEmpty(userData)) {
            connect.name = [userData objectForKey:@"name"];
            connect.email = [userData objectForKey:@"email"];
            connect.kycverified = [[userData objectForKey:@"kycverified"] boolValue];
        }
        [self.navigationController pushViewController:connect animated:YES];
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw generateotp :data :success :failure];

}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end
