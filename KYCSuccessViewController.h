//
//  KYCSuccessViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 03/04/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KYCSuccessViewController : UIViewController
- (IBAction)done:(id)sender;

@end
