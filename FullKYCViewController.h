//
//  FullKYCViewController.h
//  QuikWallet
//
//  Created by Deep on 05/03/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRHyperLabel.h"

@interface FullKYCViewController : UIViewController <UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet FRHyperLabel *LblText;
@property (weak, nonatomic) IBOutlet FRHyperLabel *aggreeLbl;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberField;
@property (weak, nonatomic) IBOutlet UITextField *aadharNumberField;
@property (weak, nonatomic) IBOutlet UITextField *otpField;
- (IBAction)getOTP:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *generateOTPBtn;
@property (weak, nonatomic) IBOutlet UISwitch *aggreeSwitch;

- (IBAction)verify:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *verifyBtn;
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic, strong) NSMutableDictionary *data;
@end
