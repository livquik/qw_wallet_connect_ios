//
//  netbankingViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by pritam soni on 14/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "netbankingViewController.h"
#import "QWConsumerConstants.h"
#import "QWActivityHelper.h"
#import "QWSdk.h"
#import "OperatorsMappingUtils.h"
#import "QWWebViewController.h"
#import "QWOnGoingPaymentViewController.h"

@interface netbankingViewController ()

@end

@implementation netbankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"CHOOSE YOUR BANK";
    
    self.bankNameField.delegate = self;
    
    self.axisBtn.tag = 401;
    self.iciciBtn.tag = 402;
    self.hdfcBtn.tag = 403;
    self.kotakBtn.tag = 404;
    self.yesBankBtn.tag = 405;
    
    [self.axisBtn addTarget:self action:@selector(setSelectedBankName:) forControlEvents:UIControlEventTouchUpInside];
    [self.iciciBtn addTarget:self action:@selector(setSelectedBankName:) forControlEvents:UIControlEventTouchUpInside];
    [self.hdfcBtn addTarget:self action:@selector(setSelectedBankName:) forControlEvents:UIControlEventTouchUpInside];
    [self.kotakBtn addTarget:self action:@selector(setSelectedBankName:) forControlEvents:UIControlEventTouchUpInside];
    [self.yesBankBtn addTarget:self action:@selector(setSelectedBankName:) forControlEvents:UIControlEventTouchUpInside];
    
    // UIPICKER VIEW IMPLEMENTATION
    
    NSArray* localarray = [[NSArray alloc] initWithObjects:NB_AXISBankNetbanking,NB_BankOfIndia,NB_BankOfMaharashtra,NB_CentralBankOfIndia,NB_CorporationBank,NB_DevelopmentCreditBank,NB_FederalBank,NB_HDFCBank,NB_ICICINetbanking,NB_IndustrialDevelopmentBankOfIndia,NB_IndianBank,NB_IndusIndBank,NB_IndianOverseasBank,NB_JammuAndKashmirBank,NB_KarnatakaBank,NB_KarurVysya,NB_SouthIndianBank,NB_UnionBankOfIndia,NB_UnitedBankOfIndia,NB_VijayaBank,NB_YesBank,NB_CityUnion,NB_CanaraBank,NB_DeutscheBank,NB_KotakBank, NB_SBofBikanerAndjaipur, NB_SBofHyderabad, NB_SBofIndia, NB_SBofMysore, NB_SBOfPatiala, NB_SBofTravancore, nil];
    
    self.bankArray = [localarray sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    
    UIPickerView *picker = [[UIPickerView alloc] init ];
    [picker setBackgroundColor: [UIColor whiteColor]];
    
    picker.dataSource = self;
    picker.delegate = self;
    
    picker.tag = 300;
    
    [picker setShowsSelectionIndicator:YES];
    [self.bankNameField setInputView:picker];
    
    UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [pickerToolBar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *pickerDoneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target: self action: @selector(removePicker)];
    
    UIBarButtonItem *pickerSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
    
    [pickerToolBar setItems:[NSArray arrayWithObjects: pickerSpace, pickerDoneBtn, nil]];
    
    [self.bankNameField setInputAccessoryView: pickerToolBar];
    
    if (!IsEmpty([self.data objectForKey:@"type"]) && [[self.data objectForKey:@"type"] isEqualToString:@"recharge"]) {
        // Do something else
    } else{
        [self setDisconnectBtn];
    }

}

- (void) setDisconnectBtn {
    UIImage *image = [[UIImage imageNamed:@"disconnect.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *disconnectBtn = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(disconnect:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:disconnectBtn, nil];
}

- (void) disconnect :(id)sender{
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)(id) = ^void(id  response){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
        [global removeObjectForKey:@"isConnected"];
        
        NSDictionary *resp = @{
                               @"code": @"209",
                               @"status": @"success"
                               };
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc] initWithDictionary:resp];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:QW_WALLET_DISCONNECT_NOTIFICATION object:self userInfo:responseData];
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw disconnect :data :success :failure];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setSelectedBankName:(UIButton*) sender
{
    switch (sender.tag) {
        case 401:
            self.bankNameField.text = NB_AXISBankNetbanking;
            break;
            
        case 402:
            self.bankNameField.text = NB_ICICINetbanking;
            break;
            
        case 403:
            self.bankNameField.text = NB_HDFCBank;
            break;
            
        case 404:
            self.bankNameField.text = NB_KotakBank;
            break;
            
        case 405:
            self.bankNameField.text = NB_YesBank;
            break;
            
        default:
            self.bankNameField.text = @"";
            break;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.bankArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.bankArray objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.bankNameField.text = [self.bankArray objectAtIndex:row];
}

- (void) removePicker {
    [self.view endEditing:YES];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (IBAction)payWithNB:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    if (IsEmpty(self.bankNameField.text)) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message: @"Please select a bank"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else{
        OperatorsMappingUtils *operatormapping = [[OperatorsMappingUtils alloc] init];
        NSString * netbankingid = [operatormapping getOperatorsId:self.bankNameField.text];
        NSDictionary *dict = @{
                               @"partnerid":[self.data objectForKey:@"partnerid"],
                               @"mobile" : [self.data objectForKey:@"mobile"],
                               @"signature":[self.data objectForKey:@"signature"],
                               @"paymentmode": @"netbanking",
                               @"netbankingcode":netbankingid
                               };
        
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
        [data setObject:self.amount forKey:@"amount"];
        
        
        void(^success)(id) = ^void(id  responsedata){
            [QWActivityHelper removeActivityIndicator:self.view];
            if ([responsedata objectForKey:@"twofactor"]!= NULL) {
                // render webview
                QWWebViewController *webView = [[QWWebViewController alloc] initWithNibName:@"QWWebView" bundle:Nil];
                webView.incomingData = self.data;
                webView.payResponseData = responsedata;
                
                [self.navigationController pushViewController:webView animated:NO];
                
            }
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        };
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw paymentUsingNetBanking:data :success :failure];
        
        QWOnGoingPaymentViewController *ongoing = [[QWOnGoingPaymentViewController alloc] init];
        [self.navigationController pushViewController:ongoing animated:NO];
        
    }
}
@end
