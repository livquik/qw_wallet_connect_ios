//
//  KYCSuccessViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 03/04/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import "KYCSuccessViewController.h"

@interface KYCSuccessViewController ()

@end

@implementation KYCSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Success";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
