//
//  AddDefaultBankAccountViewController.h
//  QuikWallet
//
//  Created by Deep on 06/03/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddDefaultBankAccountViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *accNoField;
@property (weak, nonatomic) IBOutlet UITextField *confirmAccNoField;
@property (weak, nonatomic) IBOutlet UITextField *ifscField;
- (IBAction)addDefaultAccount:(id)sender;
@property(nonatomic, assign) Boolean newPayeeFlag;
@property(nonatomic, weak) NSMutableDictionary *data;
@end
