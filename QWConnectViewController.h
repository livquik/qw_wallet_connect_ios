//
//  QWConnectViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 06/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWConnectViewController : UIViewController <UITextFieldDelegate>
@property(nonatomic, strong) NSMutableDictionary *data;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *email;
@property (nonatomic, assign) Boolean kycverified;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *otpTextField;
@property (weak, nonatomic) IBOutlet UIButton *resendBtn;

@property (strong, nonatomic) NSTimer *timer;

- (IBAction)resendOTP:(id)sender;

- (IBAction)connect:(id)sender;
@end
