//
//  QWConnectViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 06/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "QWConnectViewController.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"
#import "NSString+FormValidation.h"
#import "QWWalletViewController.h"
#import "MinKYCViewController.h"

@interface QWConnectViewController ()

@end

@implementation QWConnectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Login";
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        self.mobileTextField.text = [self.data objectForKey:@"mobile"];
        self.mobileTextField.enabled = NO;
    }
    
    if (!IsEmpty(self.name)) {
        self.nameTextField.text = self.name;
        self.nameTextField.enabled = NO;
    }
    
    if (!IsEmpty(self.email)) {
        self.emailTextField.text = self.email;
        self.emailTextField.enabled = NO;
    }
    
    self.nameTextField.delegate = self;
    self.otpTextField.delegate = self;
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.nameTextField.inputAccessoryView = keyboardDoneButtonView;
    self.otpTextField.inputAccessoryView = keyboardDoneButtonView;
    
    self.timer = nil;
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0f
                                                      target:self
                                                    selector:@selector(timerFired:)
                                                    userInfo:nil
                                                     repeats:YES];
    }

}

-(void)timerFired:(NSTimer *)_timer {
    if(self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
        
        self.resendBtn.enabled = YES;
        [self.resendBtn setTitleColor:[UIColor colorWithRed:0.00 green:0.20 blue:0.40 alpha:1.0] forState:UIControlStateNormal];
    }
    
}

- (NSString *)validateForm {
    NSString *errorMessage;
    
    if(![self.nameTextField.text isValidName])
    {
        errorMessage = @"Please enter a valid Name";
    }
    else if(![self.otpTextField.text isValidOTP])
    {
        errorMessage = @"Please enter a valid OTP";
    }else if(![self.emailTextField.text isValidEmail])
    {
        errorMessage = @"Please enter a valid Email";
    }
    
    return errorMessage;
}


- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resendOTP:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)(id) = ^void(id  data){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        self.timer = nil;
        if (!self.timer) {
            self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0f
                                                          target:self
                                                        selector:@selector(timerFired:)
                                                        userInfo:nil
                                                         repeats:YES];
        }

    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw generateotp :data :success :failure];
}

- (IBAction)connect:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSString *errorMessage = [self validateForm];
    
    if(errorMessage){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message: errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else{
        NSDictionary * dict = [[NSDictionary alloc] init];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
        if (!IsEmpty([self.data objectForKey:@"mobile"])) {
            [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
        }
        if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
            [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
        }
        if (!IsEmpty([self.data objectForKey:@"signature"])) {
            [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
        }
        
        if (!IsEmpty(self.otpTextField.text)) {
            [data setObject: self.otpTextField.text forKey:@"otp"];
        }
        
        if (!IsEmpty(self.nameTextField.text)) {
            [data setObject: self.nameTextField.text forKey:@"name"];
        }
        
        if (!IsEmpty(self.emailTextField.text)) {
            [data setObject: self.emailTextField.text forKey:@"email"];
        }
        
        if (self.kycverified) {
            void(^success)(id) = ^void(id  data){
                [QWActivityHelper removeActivityIndicator:self.view];
                
                NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
                [global setObject:@"Y" forKey:@"isConnected"];
                
                QWWalletViewController *wallet = [[QWWalletViewController alloc] init];
                wallet.data = self.data;
                [self.navigationController pushViewController:wallet animated:YES];
            };
            
            void(^failure)(NSError *) = ^void(NSError* error) {
                [QWActivityHelper removeActivityIndicator:self.view];
                
                NSDictionary *errorInfo = [error userInfo];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            };
            
            
            QWSdk *qw = [[QWSdk alloc] init];
            [qw connect :data :success :failure];
        } else{
            MinKYCViewController *minKYC = [[MinKYCViewController alloc] init];
            minKYC.params = data;
            minKYC.data = self.data;
            [self.navigationController pushViewController:minKYC animated:YES];
        }
    }
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end
