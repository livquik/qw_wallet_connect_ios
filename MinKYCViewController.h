//
//  MinKYCViewController.h
//  QuikWallet
//
//  Created by Deep on 07/02/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MinKYCViewController : UIViewController <UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *documentType;
@property (weak, nonatomic) IBOutlet UITextField *DocumentNumber;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UISwitch *agreeSwitch;

@property (nonatomic, assign) Boolean reactivateWalletFlag;

@property (nonatomic, strong) NSArray * types;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (nonatomic, strong) NSMutableDictionary * params;
@property (nonatomic, strong) NSMutableDictionary *data;

- (IBAction)submit:(id)sender;
@end
