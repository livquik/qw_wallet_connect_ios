//
//  MinKYCViewController.m
//  QuikWallet
//
//  Created by Deep on 07/02/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import "MinKYCViewController.h"
#import "NSString+FormValidation.h"
#import "QWActivityHelper.h"
#import "QWWalletViewController.h"
#import "QWSdk.h"

@interface MinKYCViewController ()

@end

@implementation MinKYCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.title = @"WALLET";
    
    self.types = @[@"PAN", @"Passport", @"Voter Id", @"Driving License"];
    
    UIPickerView *picker = [[UIPickerView alloc] init ];
    [picker setBackgroundColor: [UIColor whiteColor]];
    
    picker.dataSource = self;
    picker.delegate = self;
    [picker setShowsSelectionIndicator:YES];
    [self.documentType setInputView: picker];
    
    UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [pickerToolBar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *pickerDoneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target: self action: @selector(removePicker)];
    
    UIBarButtonItem *pickerSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
    
    [pickerToolBar setItems:[NSArray arrayWithObjects: pickerSpace, pickerDoneBtn, nil]];
    [self.documentType setInputAccessoryView: pickerToolBar];
    
    [picker reloadAllComponents];
    [picker selectRow:0 inComponent:0 animated:YES];
    
    
    self.documentType.text = @"PAN";
    
    [self.agreeSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    
    self.documentType.delegate = self;
    self.nameField.delegate = self;
    self.DocumentNumber.delegate = self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == 111) {
        return NO;
    } else{
        return YES;
    }
}

- (void)changeSwitch:(id)sender{
    if([sender isOn]){
        self.submitBtn.enabled = YES;
    } else{
        self.submitBtn.enabled = NO;
    }
}

- (void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.types count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.types objectAtIndex:row];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString * selectedText = [self.types objectAtIndex:row];
    self.documentType.text= selectedText;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (NSString *) validateForm : (NSDictionary *) formData{
    NSString *errorMessage;
    if (![[formData objectForKey:@"kyctype"] isValidName]) {
        errorMessage = @"Please enter a valid KYC type";
    }
    
    if (![[formData objectForKey:@"kycvalue"] isValidName]) {
        errorMessage = @"Please enter a valid KYC value";
    }
    
    if (![[formData objectForKey:@"name"] isValidName]) {
        errorMessage = @"Please enter a valid Name";
    }
    
    
    return errorMessage;
}

- (IBAction)submit:(id)sender {
    NSMutableDictionary *formData = [[NSMutableDictionary alloc] init];
    [formData setObject:self.documentType.text forKey:@"kyctype"];
    [formData setObject:self.DocumentNumber.text forKey:@"kycvalue"];
    [formData setObject:self.nameField.text forKey:@"name"];
    
    NSString *errorMessage = [self validateForm:formData];
    
    if (!IsEmpty(errorMessage)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message: errorMessage
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:NO completion:nil];
        
    } else{
        [self.params setObject:self.documentType.text forKey:@"kyctype"];
        [self.params setObject:self.DocumentNumber.text forKey:@"kycvalue"];
        [self.params setObject:self.nameField.text forKey:@"name"];
        
        void(^success)(id) = ^void(id  data){
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
            [global setObject:@"Y" forKey:@"isConnected"];
            
            QWWalletViewController *wallet = [[QWWalletViewController alloc] init];
            wallet.data = self.data;
            [self.navigationController pushViewController:wallet animated:YES];
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        };
        
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw connect :self.params :success :failure];
    }

}

- (void) removePicker {
    [self.view endEditing:YES];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}


@end
