//
//  QWWalletViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 06/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWWalletViewController : UIViewController <UITextFieldDelegate>
@property(nonatomic, strong) NSMutableDictionary *data;

- (IBAction)recharge:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *amountField;

@property (weak, nonatomic) IBOutlet UILabel *walletBalanceLbl;
@property (weak, nonatomic) IBOutlet UILabel *minimumBalanceLbl;
@property (weak, nonatomic) IBOutlet UIButton *Btn1;
@property (weak, nonatomic) IBOutlet UIButton *Btn2;
@property (weak, nonatomic) IBOutlet UIButton *Btn3;
@property (weak, nonatomic) IBOutlet UIButton *Btn4;

- (IBAction)handleBtnTap:(id)sender;


@end
