//
//  QWWalletCardsViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 07/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "QWWalletCardsViewController.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"
#import "QWConsumerConstants.h"
#import "NoPaymentCardViewController.h"
#import "QWCardCell.h"
#import "NSString+FormValidation.h"
#import "QWWebViewController.h"
#import "QWOnGoingPaymentViewController.h"
#define CARD_MAX_LENGTH 16

@interface QWWalletCardsViewController ()

@end

@implementation QWWalletCardsViewController

- (void) viewWillAppear:(BOOL)animated{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"ADD MONEY";
    
    [self setDisconnectBtn];
    
    // Set up datasource and delegate
    self.cardsTable.delegate = self;
    self.cardsTable.dataSource = self;
    
    [self getCards];
    
    self.ccnoField.delegate = self;
    self.nameField.delegate = self;
    self.expMonthField.delegate = self;
    self.expYearField.delegate = self;
    
    // UIPICKER VIEW IMPLEMENTATION
    self.monthArray = [[NSArray alloc] initWithObjects:@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12", nil];
    
    self.yearArray = [[NSArray alloc] initWithObjects:@"2016", @"2017", @"2018", @"2019", @"2020", @"2021", @"2022", @"2023", @"2024", @"2025", @"2026", @"2027", @"2028", @"2029", @"2030", @"2031", @"2032", @"2033", @"2034", @"2035", @"2036", @"2037", @"2038", @"2039", @"2040", nil];
    
    UIPickerView *picker = [[UIPickerView alloc] init ];
    [picker setBackgroundColor: [UIColor whiteColor]];
    
    picker.dataSource = self;
    picker.delegate = self;
    
    [picker setShowsSelectionIndicator:YES];
    [self.expMonthField setInputAccessoryView:picker];
    [self.expYearField setInputAccessoryView:picker];
    
    UIToolbar *pickerToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [pickerToolBar setTintColor:[UIColor blackColor]];
    
    UIBarButtonItem *pickerDoneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target: self action: @selector(doneClicked:)];
    
    UIBarButtonItem *pickerSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
    
    [pickerToolBar setItems:[NSArray arrayWithObjects: pickerSpace, pickerDoneBtn, nil]];
    
    [self.expMonthField setInputView:pickerToolBar];
    [self.expYearField setInputView:pickerToolBar];
    
    [self.ccnoField addTarget:self action:@selector(textFieldchanged:) forControlEvents:UIControlEventEditingChanged];

}

- (void) getCards{
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)(id) = ^void(id  response){
        [QWActivityHelper removeActivityIndicator:self.view];
        self.cards = [response objectForKey:@"paymentcards"];
        [self.cardsTable reloadData];
        
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw getWalletCards :data :success :failure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setDisconnectBtn {
    UIImage *image = [[UIImage imageNamed:@"disconnect.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *disconnectBtn = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(disconnect:)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:disconnectBtn, nil];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (void) disconnect :(id)sender{
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * dict = [[NSDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    void(^success)(id) = ^void(id  response){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
        [global removeObjectForKey:@"isConnected"];
        
        NSDictionary *resp = @{
                               @"code": @"209",
                               @"status": @"success"
                               };
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc] initWithDictionary:resp];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:QW_WALLET_DISCONNECT_NOTIFICATION object:self userInfo:responseData];
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw disconnect :data :success :failure];
}

// UITableView data source and delegate imlementation
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    if ([self.cards count] > 0) {
        self.cardsTable.backgroundView = nil;
        return 1;
    }else{
        self.cardsTable.backgroundView = nil;
        NoPaymentCardViewController *noPaymentCard = [[NoPaymentCardViewController alloc] init];
        noPaymentCard.view.frame = self.cardsTable.bounds;
        noPaymentCard.noCardFlag = true;
        self.cardsTable.backgroundView = noPaymentCard.view;
        self.cardsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        return 0;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.cards count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"myCell";
    
    NSDictionary *card = [self.cards objectAtIndex:indexPath.row];
    
    QWCardCell * cell = [self.cardsTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        [self.cardsTable registerNib:[UINib nibWithNibName:@"QWCardCell" bundle:nil] forCellReuseIdentifier:simpleTableIdentifier];
        cell = [self.cardsTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    }
    cell.ccno.text = [NSString stringWithFormat:@"%@",[card objectForKey:@"tail"]];
    cell.cardType.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_%@", [[card objectForKey:@"network"] lowercaseString]]];
    cell.logoContainerView.layer.borderColor= [[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0] CGColor];
    cell.bankName.text = [NSString stringWithFormat:@"%@", [card objectForKey:@"bank"]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([UIAlertController class]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter CVV" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            
            [textField addTarget:self
                          action:@selector(alertTextFieldDidChange:)
                forControlEvents:UIControlEventEditingChanged];
            
            textField.placeholder = @"CVV";
            textField.secureTextEntry = YES;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            
            UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
            [keyboardDoneButtonView sizeToFit];
            UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                           style:UIBarButtonItemStylePlain target:self
                                                                          action:@selector(doneClicked:)];
            [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.inputAccessoryView = keyboardDoneButtonView;
        }];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Pay"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self pay:alert.textFields.firstObject.text :indexPath.row];
                                   }];
        okAction.enabled = NO;
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                           [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
                                       }];
        
        [alert addAction:okAction];
        [alert addAction:cancelAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    } else{
        // IOS 7 Support
        // Fallback code
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter CVV"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"Pay", nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alert.tag = indexPath.row;
        UITextField *textField = [alert textFieldAtIndex:0];
        textField.placeholder = @"CVV";
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
        UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStylePlain target:self
                                                                      action:@selector(doneClicked:)];
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        textField.inputAccessoryView = keyboardDoneButtonView;
        [alert show];
    }
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;
    if (alertController)
    {
        
        NSMutableDictionary *formData = [[NSMutableDictionary alloc] init];
        
        NSString *cvv = alertController.textFields.firstObject.text;
        [formData setObject:cvv forKey:@"cvv"];
        
        NSString *errorMessage = [self validateForm:formData];
        UIAlertAction *okAction = alertController.actions.firstObject;
        
        if(errorMessage){
            okAction.enabled = NO;
        }else{
            okAction.enabled = YES;
        }
        
    }
}


- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSMutableDictionary *formData = [[NSMutableDictionary alloc] init];
    NSString *cvv = [alertView textFieldAtIndex:0].text;
    [formData setObject:cvv forKey:@"cvv"];
    
    NSString *errorMessage = [self validateForm:formData];
    
    if(errorMessage){
        return NO;
    }else{
        return YES;
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    } else{
        [self pay:[alertView textFieldAtIndex:0].text :alertView.tag];
    }
}


- (NSString *) validateForm : (NSDictionary *) formData{
    NSString *errorMessage;
    if (![[formData objectForKey:@"cvv"] checkCvvLength]) {
        errorMessage = @"Please enter a valid CVV";
    }

    return errorMessage;
}

- (NSString *)validateAddCardForm {
    NSString *errorMessage;
    
    if(![self.ccnoField.text isValidCard])
    {
        errorMessage = @"Please enter a valid card number";
    }
    else if(![self.expMonthField.text isValidMonth])
    {
        errorMessage = @"Please enter a valid month";
    }
    else if(![self.expYearField.text isValidYear])
    {
        errorMessage = @"Please enter a valid year";
    }
    
    else if(![self.cvvField.text checkCvvLength])
    {
        errorMessage = @"Please enter a valid cvv";
    }
    
    else if(![self.nameField.text isValidCardName])
    {
        errorMessage = @"Please enter a valid name";
    }
    
    return errorMessage;
}

- (IBAction)doneClicked:(id)sender
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void) pay : (NSString *) input : (NSInteger) index{
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary * card = self.cards[index];
    NSString *cvv = input;
    
    NSDictionary *dict = @{
                           @"paymentmode" : @"paymentcard",
                           @"cardtype" : @"payment",
                           };
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
        [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
    }
    if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
        [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
    }
    if (!IsEmpty([self.data objectForKey:@"signature"])) {
        [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
    }
    
    if (!IsEmpty(cvv)) {
        [data setObject: cvv forKey:@"cvv2"];
    }
    
    if (!IsEmpty(self.amount)) {
        [data setObject: self.amount forKey:@"amount"];
    }
    
    if (!IsEmpty([card objectForKey:@"cardid"])) {
        [data setObject: [card objectForKey:@"cardid"] forKey:@"cardid"];
    }
    
    
    void(^success)(id) = ^void(id  responsedata){
        
        if ([responsedata objectForKey:@"twofactor"]!= NULL) {
            // render webview
            QWWebViewController *webView = [[QWWebViewController alloc] initWithNibName:@"QWWebView" bundle:Nil];
            webView.incomingData = self.data;
            webView.payResponseData = responsedata;
            
            [self.navigationController pushViewController:webView animated:NO];
            
        } else {
            // Single factor card
            NSDictionary * paymentData  = @{ @"paymentid": [responsedata objectForKey:@"paymentid"]};
            [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_SUCCESS_NOTIFICATION  object:self userInfo:paymentData];
        }
        
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        NSDictionary *errorInfo = [error userInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    };
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw walletrecharge:data :success :failure];
    
    QWOnGoingPaymentViewController *ongoing = [[QWOnGoingPaymentViewController alloc] init];
    [self.navigationController pushViewController:ongoing animated:NO];
}

- (IBAction)rechargeWithNewCard:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSString *errorMessage = [self validateAddCardForm];
    
    if(errorMessage){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message: errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else{
        
        NSString *ccno = self.ccnoField.text;
        NSString *expMonth = self.expMonthField.text;
        NSString *expYear = self.expYearField.text;
        NSString *cvv = self.cvvField.text;
        NSString *name = self.nameField.text;
        NSString *cardType = self.cardType;
        NSString * lastFourDigit = [ccno substringFromIndex: MAX((int) [ccno length] - 4, 0)];
        
        NSDictionary *dict = @{
                               @"amount" : self.amount,
                               @"ccno":ccno,
                               @"cvv2": cvv,
                               @"type":cardType,
                               @"lastFourDigits":lastFourDigit,
                               @"expmm":expMonth,
                               @"expyyyy":expYear,
                               @"name":name,
                               @"isdefault":@"N",
                               };

        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
        if (self.saveCardSwith.isOn) {
            [data setObject:@"true" forKey:@"savecard"];
        }else{
            [data setObject:@"false" forKey:@"savecard"];
        }
        
        if (!IsEmpty([self.data objectForKey:@"mobile"])) {
            [data setObject: [self.data objectForKey:@"mobile"] forKey:@"mobile"];
        }
        if (!IsEmpty([self.data objectForKey:@"partnerid"])) {
            [data setObject: [self.data objectForKey:@"partnerid"] forKey:@"partnerid"];
        }
        if (!IsEmpty([self.data objectForKey:@"signature"])) {
            [data setObject: [self.data objectForKey:@"signature"] forKey:@"signature"];
        }

        void(^success)(id ) = ^void(id  responsedata){
            
            if ([responsedata objectForKey:@"twofactor"]!= NULL) {
                // render webview
                QWWebViewController *webView = [[QWWebViewController alloc] initWithNibName:@"QWWebView" bundle:Nil];
                webView.incomingData = self.data;
                webView.payResponseData = responsedata;
                
                [self.navigationController pushViewController:webView animated:NO];
                
            } else {
                // Single factor card
                NSDictionary * paymentData  = @{ @"paymentid": [responsedata objectForKey:@"paymentid"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_SUCCESS_NOTIFICATION  object:self userInfo:paymentData];
            }
            
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        };
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw walletrecharge:data :success :failure];
        
        QWOnGoingPaymentViewController *ongoing = [[QWOnGoingPaymentViewController alloc] init];
        [self.navigationController pushViewController:ongoing animated:NO];
    }
}

-(void) textFieldchanged :(UITextField *) sender
{
    if (self.ccnoField.text.length == 0) {
        self.cardType = nil;
        self.cardImg.image = nil;
    }
    
    NSArray * regexArray = [[NSArray alloc] initWithObjects:@"^4[0-9]{12}(?:[0-9]{3})?$",@"^5[1-5][0-9]{14}$",@"^3(?:0[0-5]|[68][0-9])[0-9]{11}$",@"^3[47][0-9]{5,}$",@"^(6[0-9]{15})$",nil];
    
    for(int i=0;i<[regexArray count];i++)
    {
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:regexArray[i]
                                      options:NSRegularExpressionCaseInsensitive
                                      error:nil];
        
        NSUInteger noofmatches = [regex numberOfMatchesInString:self.ccnoField.text options:0 range:NSMakeRange(0, [self.ccnoField.text length])];
        
        if(noofmatches)
        {
            switch (i) {
                case 0:
                {
                    UIImage *imagevisa = [UIImage imageNamed:@"icon_visa.png"];
                    [self.cardImg setImage:imagevisa];
                    self.cardType = @"VISA";
                    break;
                }
                case 1:
                {
                    UIImage *imagemaster = [UIImage imageNamed:@"icon_mastercard.png"];
                    [self.cardImg setImage:imagemaster];
                    self.cardType = @"MASTER";
                    break;
                }
                case 2:
                {
                    UIImage *imageDinners = [UIImage imageNamed:@"icon_mastercard.png"];
                    [self.cardImg setImage:imageDinners];
                    self.cardType = @"Dinners";
                    break;
                }
                case 3:
                {
                    UIImage *imageAmex = [UIImage imageNamed:@"icon_amex.png"];
                    [self.cardImg setImage:imageAmex];
                    self.cardType = @"AMEX";
                    break;
                }
                case 4:{
                    UIImage *imageRuPay = [UIImage imageNamed:@"icon_rupay.png"];
                    [self.cardImg setImage:imageRuPay];
                    self.cardType = @"RuPay";
                    break;
                }
                default:
                    break;
            }
            
        }
        
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField.tag== 0)
    {
        if (textField.text.length >= CARD_MAX_LENGTH && range.length == 0)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    return true;
    
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return [self.monthArray count];
    }
    else {
        return [self.yearArray count];
    }
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return [self.monthArray objectAtIndex:row];
    }
    else {
        return [self.yearArray objectAtIndex:row];
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (component==0) {
        self.expMonthField.text = [self.monthArray objectAtIndex:row];
    }
    else
    {
        self.expYearField.text = [self.yearArray objectAtIndex:row];
    }
}

@end
