//
//  FullKYCViewController.m
//  QuikWallet
//
//  Created by Deep on 05/03/18.
//  Copyright © 2018 LivQuik. All rights reserved.
//

#import "FullKYCViewController.h"
#import "FRHyperLabel.h"
#import "NSString+FormValidation.h"
#import "AddDefaultBankAccountViewController.h"
#import "QWActivityHelper.h"
#import "QWSdk.h"

@interface FullKYCViewController ()

@end

@implementation FullKYCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.title = @"Update KYC Details";
    
    [self setAttributedText];
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    self.nameField.delegate = self;
    self.mobileNumberField.delegate = self;
    self.otpField.delegate = self;
    self.aadharNumberField.delegate = self;
    
    [self.aggreeSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    
    if (!IsEmpty([self.data objectForKey:@"mobile"])) {
            self.mobileNumberField.text = [self.data objectForKey:@"mobile"];
    }
    
    [self.aggreeSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
}

- (void)changeSwitch:(id)sender{
    if([sender isOn]){
        self.verifyBtn.enabled = YES;
    } else{
        self.verifyBtn.enabled = NO;
    }
}


- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) setAttributedText{
    // Setup dummy attributed text
    NSString *string = @"Upgrade your wallet limit to Rs. 1,00,000. Your KYC will be verified based on the details you enter. Please refer Wallet Features for additional queries.";
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    self.LblText.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    
    
    //Step 2: Define a selection handler block
    void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://quikwallet.com/wallet-features.html"]];
    };
    
    
    //Step 3: Add link substrings
    [self.LblText setLinksForSubstrings:@[@"Wallet Features"] withLinkHandler:handler];
    [self.LblText setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
    
    // Agree Label attributed text
    NSString *txt = @"I hereby declare that the particulars given herein are true, correct and complete. I agree that I will be responsible for any cost or damages arising out of any actions undertaken by LivQuik on the basis of information provided by me, if any. Agree to our Terms & Conditions";
    NSDictionary *attr = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    self.aggreeLbl.attributedText = [[NSAttributedString alloc]initWithString:txt attributes:attr];
    
    //Step 2: Define a selection handler block
    void(^LblClickHandler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://quikwallet.com/tnc.html"]];
    };
    
    
    //Step 3: Add link substrings
    [self.aggreeLbl setLinksForSubstrings:@[@"Terms & Conditions"] withLinkHandler:LblClickHandler];
    [self.aggreeLbl setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event { /* [UIView transitionWithView:self duration:highLightAnimationTime options:UIViewAnimationOptionTransitionCrossDissolve animations:^{ self.attributedText = self.backupAttributedText; } completion:nil]; [super touchesCancelled:touches withEvent:event]; */
    
    [self touchesEnded:touches withEvent:event];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *) validateForm : (NSDictionary *) formData{
    NSString *errorMessage;
    if (![[formData objectForKey:@"name"] isValidName]) {
        errorMessage = @"Please enter a valid Name";
    }
    
    if (![[formData objectForKey:@"kycvalue"] isValidName]) {
        errorMessage = @"Please enter a valid KYC value";
    }
    
    if (![[formData objectForKey:@"mobile"] isValidMobile]) {
        errorMessage = @"Please enter a valid KYC value";
    }
    
    if (![[formData objectForKey:@"otp"] isValidOTP]) {
        errorMessage = @"Please enter a valid KYC value";
    }
    
    return errorMessage;
}

- (NSString *) validateOTPForm : (NSDictionary *) formData{
    NSString *errorMessage;
    if (![[formData objectForKey:@"name"] isValidName]) {
        errorMessage = @"Please enter a valid Name";
    }
    
    if (![[formData objectForKey:@"kycvalue"] isValidName]) {
        errorMessage = @"Please enter a valid Aadhar Number";
    }
    
    if (![[formData objectForKey:@"mobile"] isValidMobile]) {
        errorMessage = @"Please enter a valid KYC value";
    }
    
    return errorMessage;
}


- (IBAction)getOTP:(id)sender {
    
    NSMutableDictionary *formData = [[NSMutableDictionary alloc] init];
    [formData setObject:self.nameField.text forKey:@"name"];
    [formData setObject:self.mobileNumberField.text forKey:@"mobile"];
    [formData setObject:self.aadharNumberField.text forKey:@"kycvalue"];
    
    NSString *errorMessage = [self validateOTPForm:formData];
    
    if (!IsEmpty(errorMessage)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message: errorMessage
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:NO completion:nil];
        
    } else{
        // Display spinner
        [QWActivityHelper displayActivityIndicator:self.view];
        
        void(^success)(id) = ^void(id  data){
            self.generateOTPBtn.enabled = NO;
            [self.generateOTPBtn setTitle:@"Resend OTP" forState:UIControlStateNormal];
            [self.generateOTPBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            
            self.timer = nil;
            self.nameField.enabled = NO;
            self.aadharNumberField.enabled = NO;
            
            [QWActivityHelper removeActivityIndicator:self.view];
            
            if (!self.timer) {
                self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                                              target:self
                                                            selector:@selector(timerFired:)
                                                            userInfo:nil
                                                             repeats:YES];
            }
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        };
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.data];
        [data setObject:self.nameField.text forKey:@"name"];
        [data setObject:self.mobileNumberField.text forKey:@"mobile"];
        [data setObject:self.aadharNumberField.text forKey:@"kycvalue"];
        
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw sendKycOtp:data :success :failure];

    }
}

- (IBAction)verify:(id)sender {
    NSMutableDictionary *formData = [[NSMutableDictionary alloc] init];
    [formData setObject:self.nameField.text forKey:@"name"];
    [formData setObject:self.mobileNumberField.text forKey:@"mobile"];
    [formData setObject:self.aadharNumberField.text forKey:@"kycvalue"];
    [formData setObject:self.otpField.text forKey:@"otp"];
    
    NSString *errorMessage = [self validateForm:formData];
    
    if (!IsEmpty(errorMessage)) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message: errorMessage
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:NO completion:nil];
        
    } else{
        // Display spinner
        [QWActivityHelper displayActivityIndicator:self.view];
        
        self.generateOTPBtn.enabled = NO;
        [self.generateOTPBtn setTitle:@"Resend OTP" forState:UIControlStateNormal];
        [self.generateOTPBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        void(^success)(id) = ^void(id  data){
            [QWActivityHelper removeActivityIndicator:self.view];
            AddDefaultBankAccountViewController *bankAccount = [[AddDefaultBankAccountViewController alloc] init];
            bankAccount.data = self.data;
            [self.navigationController pushViewController:bankAccount animated:YES];
        };
        
        void(^failure)(NSError *) = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];
            
            NSDictionary *errorInfo = [error userInfo];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        };
        
        
        
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:self.data];
        [data setObject:self.nameField.text forKey:@"name"];
        [data setObject:self.mobileNumberField.text forKey:@"mobile"];
        [data setObject:self.aadharNumberField.text forKey:@"kycvalue"];
        [data setObject:self.otpField.text forKey:@"otp"];
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw updateKYC:data :success :failure];
    }
}

-(void)timerFired:(NSTimer *)_timer {
    
    if(self.timer)
    {
        [self.timer invalidate];
        self.timer = nil;
        
        self.generateOTPBtn.enabled = YES;
        [self.generateOTPBtn setTitleColor:[UIColor colorWithRed:78.0/256.0 green:157.0/256.0 blue:196.0/256.0 alpha:1.0] forState:UIControlStateNormal];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 13 && textField.text.length >= 12 && range.length == 0) {
        return NO;
    } else if (textField.tag == 14 && textField.text.length >= 6 && range.length == 0){
        return NO;
    }
    else
    {
        return YES;
    }
}


@end
