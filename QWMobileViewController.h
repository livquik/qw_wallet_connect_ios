//
//  QWMobileViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 31/08/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWMobileViewController : UIViewController

@property(nonatomic, strong) NSMutableDictionary *data;

@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLbl;

- (IBAction)generateOTP:(id)sender;

@end
