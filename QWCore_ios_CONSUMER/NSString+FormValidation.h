//
//  NSString+FormValidation.h
//  QuikWallet
//
//  Created by Gunendu Das on 28/05/14.
//  Copyright (c) 2014 LivQuik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormValidation)
- (BOOL)isValidEmail;
- (BOOL)isValidPassword;
- (BOOL) isConfirmPassWord:(NSString*) password;
- (BOOL)isValidName;
- (BOOL)isValidAmount;
- (BOOL)isValidCard;
- (BOOL)isValidCardName;
- (BOOL)isValidMonth;
- (BOOL)isValidYear;
- (BOOL)isValidMobile;
- (BOOL)checkCvvLength;
- (BOOL)isValidProvider;
- (BOOL) isValidPincode;
- (BOOL) isValidOTP;
- (BOOL)isValidAccount;
- (BOOL)isValidIFSC;
@end
