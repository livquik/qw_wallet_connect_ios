//
//  QWOnGoingPaymentViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 23/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import "QWOnGoingPaymentViewController.h"

@interface QWOnGoingPaymentViewController ()

@end

@implementation QWOnGoingPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.spinner startAnimating];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.spinner  stopAnimating];
}

@end
