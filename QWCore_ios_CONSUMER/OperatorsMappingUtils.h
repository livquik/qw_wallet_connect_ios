//
//  OperatorsMappingUtils.h
//  QuikWallet
//
//  Created by Gunendu Das on 15/01/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OperatorsMappingUtils : NSObject

@property (strong,nonatomic) NSMutableDictionary *map;

- (NSString*) getOperatorsId :(NSString*) operatorname;

@end
