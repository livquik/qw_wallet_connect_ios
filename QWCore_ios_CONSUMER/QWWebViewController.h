//
//  QWWebViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by dilip on 28/12/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWWebViewController : UIViewController <UIWebViewDelegate, UIAlertViewDelegate>

@property (nonatomic, copy) NSString * paymentid;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property(nonatomic, strong) NSMutableDictionary *incomingData;

@property(nonatomic, strong) NSMutableDictionary *payResponseData;

@property(nonatomic, strong) NSMutableDictionary *prepaidRechargeResponseData;


@end

