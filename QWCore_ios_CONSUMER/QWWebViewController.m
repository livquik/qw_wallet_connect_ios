//
//  QWWebViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by dilip on 28/12/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import "QWWebViewController.h"
#import "QWConsumerConstants.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"

@implementation QWWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    self.webView.delegate = self;
    
    self.navigationItem.title = @"Processing your payment";
    
    
    if (!IsEmpty(self.payResponseData)) {
        self.paymentid = [self.payResponseData objectForKey:@"paymentid"];
    } else {
        self.paymentid = [self.prepaidRechargeResponseData objectForKey:@"rechargeid"];
    }
    
    // load twofactorurl
    
    NSString *url;
    if (!IsEmpty([self.payResponseData objectForKey:@"twofactor"])) {
        url = [self.payResponseData objectForKey:@"twofactor"];
    } else {
        url = [self.prepaidRechargeResponseData objectForKey:@"twofactor"];
    }
    NSURL *nsurl = [NSURL URLWithString:url];
    NSURLRequest *requestbody = [NSURLRequest requestWithURL:nsurl];
    [self.webView loadRequest:requestbody];
    
}

-(void) popBack {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Are you sure you want to cancel?"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:@"Cancel", nil];
    [alert show];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString* js = [NSString stringWithFormat:@"var meta = document.createElement('meta'); " \
                    "meta.setAttribute( 'name', 'viewport' ); " \
                    "meta.setAttribute( 'content', 'width = device-width, initial-scale = %f, user-scalable = yes' ); " \
                    "document.getElementsByTagName('head')[0].appendChild(meta)",0.8];
    
    [self.webView stringByEvaluatingJavaScriptFromString: js];
}



- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if(navigationType == UIWebViewNavigationTypeFormSubmitted)
    {
        NSString *stringUrl = [request.URL absoluteString];
        
        if([stringUrl rangeOfString:@"#2fr/success"].location!=NSNotFound)
        {
            
            if (!IsEmpty(self.payResponseData)) {
                [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_SUCCESS_NOTIFICATION  object:self userInfo:self.payResponseData];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:QW_RECHARGE_SUCCESS_NOTIFICATION  object:self userInfo:self.prepaidRechargeResponseData];
            }
            
            
            
        }
        else if([stringUrl rangeOfString:@"#2fr/failed"].location!=NSNotFound)
        {
            if (!IsEmpty(self.payResponseData)) {
                [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_FRAILURE_NOTIFICATION  object:self userInfo:@{@"NSLocalizedDescription":@"something went wrong! Please try again."}];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:QW_RECHARGE_FAILURE_NOTIFICATION  object:self userInfo:@{@"NSLocalizedDescription":@"something went wrong! Please try again."}];
            }
            
            
        }
        
    }
    return YES;
}


static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end
