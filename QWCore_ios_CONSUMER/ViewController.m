//
//  ViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by dilip on 11/08/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import "ViewController.h"
#import "QWWalletConnect.h"
#import "QWConsumerConstants.h"
#import "QWSdk.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(success:) name:QW_PAYMENT_SUCCESS_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(failure:) name:QW_PAYMENT_FRAILURE_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(rechargesuccess:) name:QW_RECHARGE_SUCCESS_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(rechargefailure:) name:QW_RECHARGE_FAILURE_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(paymentCanceled:) name:QW_PAYMENT_CANCEL_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(walletDisconnected:) name:QW_WALLET_DISCONNECT_NOTIFICATION object:nil];
    
    self.navigationItem.title = @"QW WALLET CONNECT";
}

- (IBAction)pay:(id)sender {
    NSDictionary *dict = @{
                           @"signature": TEST_SINGATURE,
                           @"partnerid": TEST_PARTNER_ID,
                           @"mobile": @"2186434499",
                           @"amount": @"100"
                           };
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    QWWalletConnect * qw = [[QWWalletConnect alloc] init];
    [qw pay:data :self.navigationController];
}

- (IBAction)fullKYC:(id)sender {
    /* Navigation Bar Customization */
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[self.navigationController navigationBar] setBarStyle:UIStatusBarStyleLightContent];
    /* End of Navigation Bar Styling */
    
    
    NSDictionary *dict = @{
                           @"signature": TEST_SINGATURE,
                           @"partnerid": TEST_PARTNER_ID,
                           @"mobile": @"2186434499",
                           @"type": @"details"
                           };
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary: dict];
    
    QWWalletConnect * qw = [[QWWalletConnect alloc] init];
    [qw fullKYC:data :self.navigationController];
}

- (IBAction)walletConnect:(id)sender {
    /* Navigation Bar Customization */
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.17 green:0.24 blue:0.31 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = NO;
    // [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[self.navigationController navigationBar] setBarStyle:UIStatusBarStyleLightContent];
    /* End of Navigation Bar Styling */
    
    NSDictionary *dict = @{
                           @"signature": TEST_SINGATURE,
                           @"partnerid": TEST_PARTNER_ID,
                           @"mobile": @"2186434499",
                           @"type": @"details"
                           };
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary: dict];
    
    QWWalletConnect * qw = [[QWWalletConnect alloc] init];
    [qw walletConnect:data :self.navigationController];
}


-(void) success:(NSNotification*)notification{
    NSLog(@"Received notification");
    NSDictionary* dict = notification.userInfo;
    NSLog(@"Success data %@", dict);
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: @"Your payment was successful!"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

-(void) failure:(NSNotification*)notification{
    NSLog(@"Received notification for payment failure %@",notification);
    
    NSDictionary* dict = notification.userInfo;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: [dict objectForKey:@"NSLocalizedDescription"]
                                                    delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
    [alert show];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) rechargesuccess:(NSNotification*)notification{
    NSLog(@"Received notification for recharge succcess");
    NSDictionary* data = notification.userInfo;
    NSLog(@"Success data %@", data);
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: @"Your recharge was successful!"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void) rechargefailure:(NSNotification*)notification{
    NSLog(@"Received notification for recharge failure");
    NSDictionary* data = notification.userInfo;
    NSLog(@"DATA %@", data);
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: [data objectForKey:@"NSLocalizedDescription"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void) paymentCanceled:(NSNotification*)notification{
    NSLog(@"Received notification for payment canceled event");
    
    NSDictionary* data = notification.userInfo;
    NSLog(@"DATA %@", data);
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: [data objectForKey:@"NSLocalizedDescription"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) walletDisconnected: (NSNotification *) notification {
    NSDictionary* data = notification.userInfo;
    NSLog(@"DATA %@", data);
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: @"Successfully disconnected wallet"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

}

@end
