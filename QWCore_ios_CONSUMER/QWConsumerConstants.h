//
//  QWConsumerConstants.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 23/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QWConsumerConstants : NSObject

//netbanking mappings
FOUNDATION_EXPORT NSString * const NB_AXISBankNetbanking ;
FOUNDATION_EXPORT NSString * const NB_BankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_BankOfMaharashtra ;
FOUNDATION_EXPORT NSString * const NB_CentralBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_CorporationBank ;
FOUNDATION_EXPORT NSString * const NB_DevelopmentCreditBank ;
FOUNDATION_EXPORT NSString * const NB_FederalBank ;
FOUNDATION_EXPORT NSString * const NB_HDFCBank ;
FOUNDATION_EXPORT NSString * const NB_ICICINetbanking ;
FOUNDATION_EXPORT NSString * const NB_IndustrialDevelopmentBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_IndianBank;
FOUNDATION_EXPORT NSString * const NB_IndusIndBank;
FOUNDATION_EXPORT NSString * const NB_IndianOverseasBank ;
FOUNDATION_EXPORT NSString * const NB_JammuAndKashmirBank ;
FOUNDATION_EXPORT NSString * const NB_KarnatakaBank ;
FOUNDATION_EXPORT NSString * const NB_KarurVysya ;
FOUNDATION_EXPORT NSString * const NB_SBofBikanerAndjaipur ;
FOUNDATION_EXPORT NSString * const NB_SBofHyderabad ;
FOUNDATION_EXPORT NSString * const NB_SBofIndia ;
FOUNDATION_EXPORT NSString * const NB_SBofMysore ;
FOUNDATION_EXPORT NSString * const NB_SBofTravancore ;
FOUNDATION_EXPORT NSString * const NB_SouthIndianBank ;
FOUNDATION_EXPORT NSString * const NB_UnionBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_UnitedBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_VijayaBank ;
FOUNDATION_EXPORT NSString * const NB_YesBank  ;
FOUNDATION_EXPORT NSString * const NB_CityUnion ;
FOUNDATION_EXPORT NSString * const NB_CanaraBank  ;
FOUNDATION_EXPORT NSString * const NB_SBOfPatiala ;
FOUNDATION_EXPORT NSString * const NB_CitiBankNetbanking ;
FOUNDATION_EXPORT NSString * const NB_DeutscheBank ;
FOUNDATION_EXPORT NSString * const NB_KotakBank ;


// notifications for end app to capture
#define QW_PAYMENT_SUCCESS_NOTIFICATION     @"qw_payment_success_notification"
#define QW_PAYMENT_FRAILURE_NOTIFICATION    @"qw_payment_failure_notification"
#define QW_PAYMENT_CANCEL_NOTIFICATION      @"qw_payment_cancel_notification"

// notification for recharge / SDK internal events
#define QW_RECHARGE_SUCCESS_NOTIFICATION     @"qw_recharge_success_notification"
#define QW_RECHARGE_FAILURE_NOTIFICATION    @"qw_recharge_failure_notification"
#define QW_RECHARGE_CANCEL_NOTIFICATION      @"qw_recharge_cancel_notification"

// Wallet Notifications
#define QW_WALLET_DISCONNECT_NOTIFICATION     @"qw_wallet_disconnect_notification"


// test data
// #define TEST_MOBILE @"7700000078"
// #define TEST_PARTNER_ID @"112"
// #define TEST_SINGATURE @"d93776ba8bed286ff0c6de85fd9b412693479c1da60fd33d66a53735c118f3fa"
// #define TEST_SINGATURE @"65105adb9c971dac71cc1506c551a62844482b263074433e29fc0fa162076777"

// test data
#define TEST_MOBILE @"2186434499"
#define TEST_PARTNER_ID @"112"
#define TEST_SINGATURE @"e77e6b2ead6ca76ad9911847534a039e8ec8eb7548e7f99dc46291141b6d5b39"


//UAT - from android sdk
//#define TEST_MOBILE @"9740401023"
//#define TEST_PARTNER_ID @"10"
//#define TEST_SINGATURE @"56fc15a0b85cc329f012add3e19f214febaede2d2b78eb79907b840c65bccbf9"

// For setting intended action of end app .. payment or show card etc... from CLINET APP
#define INTENDED_ACTION_FROM_CLIENT @"intended_action_from_client"
#define INTENDED_CLIENT_ACTION_CONDUCT_PAYMENT @"intended_client_action_conduct_payment"
#define INTENDEDN_CLIENT_ACTION_SHOW_CARDS @"intended_client_action_show_cards"

// For setting intended action within SDK .. prepaid recharge etc... from QW SDK
#define INTENDED_SDK_ACTION @"intended_sdk_action"
#define INTENDED_SDK_ACTION_PREPAID_RECHARGE @"intended_sdk_action_prepaid_recharge"

@end
