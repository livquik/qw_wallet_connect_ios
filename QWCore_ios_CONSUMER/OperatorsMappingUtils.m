//
//  OperatorsMappingUtils.m
//  QuikWallet
//
//  Created by Gunendu Das on 15/01/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import "OperatorsMappingUtils.h"
#import "QWConsumerConstants.h"

@implementation OperatorsMappingUtils
@synthesize map;

-(id) init {
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    //netbanking mappings
    [data setObject:@"AXIB" forKey:NB_AXISBankNetbanking];
    [data setObject:@"BOIB" forKey:NB_BankOfIndia];
    [data setObject:@"BOMB" forKey:NB_BankOfMaharashtra];
    [data setObject:@"CBIB" forKey:NB_CentralBankOfIndia];
    [data setObject:@"CRPB" forKey:NB_CorporationBank];
    [data setObject:@"DCBB" forKey:NB_DevelopmentCreditBank];
    [data setObject:@"FEDB" forKey:NB_FederalBank];
    [data setObject:@"HDFB" forKey:NB_HDFCBank];
    [data setObject:@"ICIB" forKey:NB_ICICINetbanking];
    [data setObject:@"IDBB" forKey:NB_IndustrialDevelopmentBankOfIndia];
    [data setObject:@"INDB" forKey:NB_IndianBank];
    [data setObject:@"INIB" forKey:NB_IndusIndBank];
    [data setObject:@"INOB" forKey:NB_IndianOverseasBank];
    [data setObject:@"JAKB" forKey:NB_JammuAndKashmirBank];
    [data setObject:@"KRKB" forKey:NB_KarnatakaBank];
    [data setObject:@"KRVB" forKey:NB_KarurVysya];
    [data setObject:@"SBBJB" forKey:NB_SBofBikanerAndjaipur];
    [data setObject:@"SBHB" forKey:NB_SBofHyderabad];
    [data setObject:@"SBIB" forKey:NB_SBofIndia];
    [data setObject:@"SBMB" forKey:NB_SBofMysore];
    [data setObject:@"SBTB" forKey:NB_SBofTravancore];
    [data setObject:@"SOIB" forKey:NB_SouthIndianBank];
    [data setObject:@"UBIB" forKey:NB_UnionBankOfIndia];
    [data setObject:@"UNIB" forKey:NB_UnitedBankOfIndia];
    [data setObject:@"VJYB" forKey:NB_VijayaBank];
    [data setObject:@"YESB" forKey:NB_YesBank];
    [data setObject:@"CUBB" forKey:NB_CityUnion];
    [data setObject:@"CABB" forKey:NB_CanaraBank];
    [data setObject:@"SBPB" forKey:NB_SBOfPatiala];
    //[data setObject:@"CITNB" forKey:NB_CitiBankNetbanking];
    [data setObject:@"DSHB" forKey:NB_DeutscheBank];
    [data setObject:@"162B" forKey:NB_KotakBank];
    
    [data setObject:@"SBBJB" forKey:NB_SBofBikanerAndjaipur];
    [data setObject:@"SBHB" forKey:NB_SBofHyderabad];
    [data setObject:@"SBIB" forKey:NB_SBofIndia];
    [data setObject:@"SBMB" forKey:NB_SBofMysore];
    [data setObject:@"SBTB" forKey:NB_SBofTravancore];
    [data setObject:@"SBPB" forKey:NB_SBOfPatiala];

    map = data;
    
    return self;
}

- (NSString*)getOperatorsId :(NSString*)operatorname
{
   NSString *operator = [map objectForKey:operatorname];
    return operator;
}

@end
