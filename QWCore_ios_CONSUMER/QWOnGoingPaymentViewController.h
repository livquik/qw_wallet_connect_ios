//
//  QWOnGoingPaymentViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 23/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWOnGoingPaymentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end
