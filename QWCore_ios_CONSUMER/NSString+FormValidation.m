//
//  NSString+FormValidation.m
//  QuikWallet
//
//  Created by Gunendu Das on 28/05/14.
//  Copyright (c) 2014 LivQuik. All rights reserved.
//

#import "NSString+FormValidation.h"
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]


@implementation NSString (FormValidation)

- (NSMutableArray *) toCharArray {
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[self length]];
    for (int i=0; i < [self length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [self characterAtIndex:i]];
        [characters addObject:ichar];
    }
    
    return characters;
}

- (BOOL) luhnCheck:(NSString *)stringToTest {
    
    NSMutableArray *stringAsChars = [stringToTest toCharArray];
    
    BOOL isOdd = YES;
    int oddSum = 0;
    int evenSum = 0;
    
    for (int i = (int)[stringToTest length] - 1; i >= 0; i--) {
        
        int digit = [(NSString *)[stringAsChars objectAtIndex:i] intValue];
        
        if (isOdd)
            oddSum += digit;
        else
            evenSum += digit/5 + (2*digit) % 10;
        
        isOdd = !isOdd;
    }
    
    return ((oddSum + evenSum) % 10 == 0);
}

- (BOOL) isValidEmail {
    NSString *regex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [emailPredicate evaluateWithObject:self];
}

- (BOOL) isValidName {
    return ([allTrim(self) length] > 0 );
}

- (BOOL) isValidPassword {
    return (self.length >=8);
}

-(BOOL) isConfirmPassWord:(NSString*) password {
    
     if([self isEqualToString:password])
         return true;
     else
         return false;
 }

- (BOOL) isValidAmount {
    return (self.intValue >0);
}

- (BOOL) isValidCard {
    return ([self luhnCheck:self] && self.length >= 15);
}

- (BOOL) isValidCardName {
    return (self.length > 0);
}

- (BOOL) isValidOTP{
    NSScanner *scanner = [NSScanner scannerWithString:self];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return (self.length > 0 && isNumeric > 0);
}

- (BOOL) isValidMonth {
    NSScanner *scanner = [NSScanner scannerWithString:self];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return (isNumeric > 0);
}

- (BOOL) isValidYear {
    NSScanner *scanner = [NSScanner scannerWithString:self];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return (isNumeric > 0);
}

- (BOOL)isValidMobile {
    return (self.length>=10);
    
}

- (BOOL)checkCvvLength {
    return (self.length>=3 && self.length <= 4);
}

- (BOOL)isValidProvider {
    return (self.length>0);
}

- (BOOL) isValidPincode{
    NSScanner *scanner = [NSScanner scannerWithString:self];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return (isNumeric > 0 && self.length == 6);
}

- (BOOL)isValidAccount {
    return (self.length >= 10 && self.length <= 20);
}

- (BOOL)isValidIFSC {
    return (self.length == 11);
}



@end
