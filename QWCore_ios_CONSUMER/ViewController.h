//
//  ViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by dilip on 11/08/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)walletConnect:(id)sender;

@end

