//
//  AppDelegate.m
//  QWCore_ios_CONSUMER
//
//  Created by dilip on 11/08/15.
//  Copyright (c) 2015 LivQuik. All rights reserved.
//

#import "AppDelegate.h"
#import "QWSdk.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [QuikWallet setDefaultConfig:@"test"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

@end
