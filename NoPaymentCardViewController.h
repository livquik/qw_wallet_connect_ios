//
//  NoPaymentCardViewController.h
//  QuikWallet
//
//  Created by Monideep Purkayastha on 19/01/16.
//  Copyright © 2016 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoPaymentCardViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *msg;
@property (nonatomic, assign)Boolean debitCardFlag;
@property (nonatomic, assign)Boolean noCardFlag;
@end
