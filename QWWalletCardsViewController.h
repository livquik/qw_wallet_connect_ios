//
//  QWWalletCardsViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 07/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWWalletCardsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property(nonatomic, strong) NSMutableDictionary *data;
@property(nonatomic, copy) NSString * amount;
@property(nonatomic, copy) NSMutableArray * cards;

@property (weak, nonatomic) IBOutlet UITableView *cardsTable;

@property (weak, nonatomic) IBOutlet UITextField *ccnoField;
@property (weak, nonatomic) IBOutlet UITextField *cvvField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UISwitch *saveCardSwith;
@property (weak, nonatomic) IBOutlet UIButton *payNowBtn;
@property (weak, nonatomic) IBOutlet UITextField *expMonthField;
@property (weak, nonatomic) IBOutlet UITextField *expYearField;

@property (nonatomic,assign) NSString *cardType;
@property (weak, nonatomic) IBOutlet UIImageView *cardImg;
- (IBAction)rechargeWithNewCard:(id)sender;

@property (nonatomic, copy) NSArray *monthArray;
@property (nonatomic, copy) NSArray *yearArray;

@end
