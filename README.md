## QW Wallet Connect SDK Documentation

### Getting Started
First clone the repository using git
```sh
$ git clone https://meaww@bitbucket.org/livquik/qw_wallet_connect_ios.git
$ cd qw_wallet_connect_ios
$ open QWCore_ios_CONSUMER.xcodeproj 
```
Once the project is opened in XCode drag and drop ```WalletConnectSDK``` folder into your project.

Note: A sample project is also included in the repository.

### Initialize QWSdk
In your ```AppDelegate.m``` file first import ```QWSdk.h``` file.
```objc
#import "QWSdk.h"
```
And add the following line to the start of ```didFinishLaunchingWithOptions```.
```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Optional - This step is needed only for testing your integration, for production environment this is not needed.
    [QuikWallet setDefaultConfig:@"test"]; 
    return YES;
}
```
In your view Controller first import the following header files.
```objc
#import "QWWalletConnect.h"
#import "QWConsumerConstants.h"
#import "QWSdk.h"
```
### SDK Methods

- ##### Wallet connect
```objc
NSDictionary *dict = @{
    @"signature": TEST_SINGATURE,
    @"partnerid": TEST_PARTNER_ID,
    @"mobile": @"2186434499",
    @"type": @"details"
};
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary: dict];
    
    QWWalletConnect * qw = [[QWWalletConnect alloc] init];
    [qw walletConnect:data :self.navigationController];
```

- ##### Direct Debit
```objc
NSDictionary *dict = @{
    @"signature": TEST_SINGATURE,
    @"partnerid": TEST_PARTNER_ID,
    @"mobile": @"2186434499",
    @"amount": @"100"
};
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    QWWalletConnect * qw = [[QWWalletConnect alloc] init];
    [qw pay:data :self.navigationController];
```

- ##### Full KYC
```objc
NSDictionary *dict = @{
                           @"signature": TEST_SINGATURE,
                           @"partnerid": TEST_PARTNER_ID,
                           @"mobile": @"2186434499",
                           @"type": @"details"
                           };
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary: dict];
    
    QWWalletConnect * qw = [[QWWalletConnect alloc] init];
    [qw fullKYC:data :self.navigationController];
```

### Add observers
Now inside the viewDidLoad method add the following observer that will respond to SDK events.

```objc
[[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(success:) name:QW_PAYMENT_SUCCESS_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(failure:) name:QW_PAYMENT_FRAILURE_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(rechargesuccess:) name:QW_RECHARGE_SUCCESS_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(rechargefailure:) name:QW_RECHARGE_FAILURE_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(paymentCanceled:) name:QW_PAYMENT_CANCEL_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(walletDisconnected:) name:QW_WALLET_DISCONNECT_NOTIFICATION object:nil];
```
Example selector

```objc
-(void) success:(NSNotification*)notification{
    NSLog(@"Received notification");
    NSDictionary* dict = notification.userInfo;
    NSLog(@"Success data %@", dict);
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: @"Your payment was successful!"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
}

-(void) failure:(NSNotification*)notification{
    NSLog(@"Received notification for payment failure %@",notification);
    NSDictionary* dict = notification.userInfo;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message: [dict objectForKey:@"NSLocalizedDescription"]
                                                    delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
    [alert show];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
```

From here, you can hit the Run button, or hit⌘Ron your keyboard and you’ll see the simulator pop up with the welcome screen.
