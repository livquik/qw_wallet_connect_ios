//
//  netbankingViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by pritam soni on 14/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface netbankingViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *payNowNetbanking;
@property (weak, nonatomic) IBOutlet UITextField *bankNameField;
@property (weak, nonatomic) IBOutlet UIButton *axisBtn;
@property (weak, nonatomic) IBOutlet UIButton *iciciBtn;
@property (weak, nonatomic) IBOutlet UIButton *hdfcBtn;
@property (weak, nonatomic) IBOutlet UIButton *kotakBtn;
@property (weak, nonatomic) IBOutlet UIButton *yesBankBtn;
- (IBAction)payWithNB:(id)sender;

@property (nonatomic, copy) NSArray *bankArray;
@property(nonatomic, strong) NSMutableDictionary *data;
@property(nonatomic, copy) NSString * amount;
@end
