//
//  WalletConnect.h
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 06/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QWWalletConnect : NSObject

- (void) walletConnect : (NSMutableDictionary *) data : (UINavigationController *) navigation;
- (void) fullKYC : (NSMutableDictionary *) data : (UINavigationController *) navigation;
- (void) pay : (NSMutableDictionary *) data : (UINavigationController *) navigation;
@end
