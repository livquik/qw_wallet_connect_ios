//
//  WalletConnect.m
//  QWCore_ios_CONSUMER
//
//  Created by Deep on 06/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import "QWWalletConnect.h"
#import "QWMobileViewController.h"
#import "QWWalletViewController.h"
#import "IQKeyboardManager/IQKeyboardManager.h"
#import "FullKYCViewController.h"
#import "QWSdk.h"
#import "QWConsumerConstants.h"

@implementation QWWalletConnect

- (void) walletConnect : (NSMutableDictionary *) data : (UINavigationController *) navigation{
    [IQKeyboardManager sharedManager].enable = true;
    
    
    NSUserDefaults *global = [NSUserDefaults standardUserDefaults];
    
    if (!IsEmpty([global objectForKey:@"isConnected"]) && [[global objectForKey:@"isConnected"] isEqualToString:@"Y"]) {
        QWWalletViewController *wallet = [[QWWalletViewController alloc] init];
        wallet.data = data;
        [navigation pushViewController:wallet animated:YES];
        
    } else{
        QWMobileViewController *mobile = [[QWMobileViewController alloc] init];
        mobile.data = data;
        [navigation pushViewController:mobile animated:YES];
    }
}

- (void) fullKYC : (NSMutableDictionary *) data : (UINavigationController *) navigation{
    [IQKeyboardManager sharedManager].enable = true;
    
    FullKYCViewController *fullKYC = [[FullKYCViewController alloc] init];
    fullKYC.data = data;
    [navigation pushViewController:fullKYC animated:YES];
}

- (void) pay : (NSMutableDictionary *) data : (UINavigationController *) navigation{
    void(^success)(id) = ^void(id  responsedata){
        [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_SUCCESS_NOTIFICATION  object:self userInfo:responsedata];
    };
    
    void(^failure)(NSError *) = ^void(NSError* error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_FRAILURE_NOTIFICATION  object:self userInfo:[error userInfo]];
    };
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw walletpay:data :success :failure];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end
