//
//  selectpaymenttypeViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by pritam soni on 14/09/17.
//  Copyright © 2017 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface selectpaymenttypeViewController : UIViewController
@property(nonatomic, strong) NSMutableDictionary *data;
@property(nonatomic, copy) NSString * amount;
@property(nonatomic, copy) NSMutableArray *modes;

@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIView *netbankingView;

@end
